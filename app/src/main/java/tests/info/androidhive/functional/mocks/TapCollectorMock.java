package tests.info.androidhive.functional.mocks;

import java.util.List;

import info.androidhive.actionbar.BleBroadcast;
import info.androidhive.functional.BleDevice;

/**
 * Created by hjacker on 31.05.15.
 */
public class TapCollectorMock implements BleBroadcast {

    public Integer tapCnt = 0;

    @Override
    public void doBroadcastScanInProgress() {

    }

    @Override
    public void doBroadcastScanDone(List<BleDevice> discoveredDevices) {

    }

    @Override
    public void doBroadcastBleEnable() {

    }

    @Override
    public void doBroadcastBleNotEnable() {

    }

    @Override
    public void doBroadcastConnectionToNonValidDevice() {

    }

    @Override
    public void doBroadcastSuccessfulConnectionToDevice(String deviceName) {

    }

    @Override
    public void doBroadcastDeviceDisconnected(String deviceName) {

    }

    @Override
    public void odBroadcastOnRssiUpdate(Integer RSSI) {

    }

    @Override
    public void doBroadcastOnTapDetected(byte [] f) {
        tapCnt++;
    }

    @Override
    public void doBroadcastOnTripleTapDetected(byte [] f) {

    }

    @Override
    public void doBroadcastOnReadPedometerData(byte[] bytes) {

    }

    @Override
    public void doBroadcastOnReadPedometerDriver(byte[] bytes) {

    }

    @Override
    public void doBroadcastOnReadBleDriver(byte[] bytes) {

    }

    @Override
    public void doBroadcastOnReadRtcDriver(byte[] bytes) {

    }

    @Override
    public void doBroadcastOnReadRtcData(byte[] bytes) {

    }

    @Override
    public void doBroadcastOnFlashMemoryData(byte[] bytes) {

    }
}
