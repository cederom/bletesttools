package info.androidhive.telephony;

import android.app.KeyguardManager;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.actionbar.GlobalActivity;
import info.androidhive.actionbar.PersonalData;
import info.androidhive.actionbar.lock.MyAdmin;
import info.androidhive.applicationpipes.TelephonyEventsReceiver;
import info.androidhive.applicationpipes.result.SmsEventResult;
import info.androidhive.applicationpipes.result.TelephonyEventResult;
import info.androidhive.applicationpipes.wrappers.SmsEventWrapper;
import info.androidhive.applicationpipes.wrappers.TelephonyWrapper;
import info.androidhive.communication.AppMessages;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.functional.BleDevice;
import info.androidhive.systemevents.PhoneIntentReceiver;
import info.androidhive.systemevents.SMSBroadcastReceiver;

/**
 * Created by hjacker on 20.06.15.
 */
public class TelephonyService extends Service implements TelephonyEventsReceiver {

    byte [] emergencyPayload = "STE 3 143 122 254 0 0 255 248 255 40\r\n".getBytes();
    byte [] cleanPayload = "STE 0 0 0 0 0 0 0 0 0 0\r\n".getBytes();
    byte [] smsPayload = "STE 1 32 0 0 2 255 121 210 34 23\r\n".getBytes();
    byte [] callPayload =  "STE 1 17 255 255 255 255 109 209 173 33\r\n".getBytes();
    byte[] afterCallPayload = "STE 3 17 0 255 0 0 255 249 172 0\r\n".getBytes();
    byte[] kaleidoscopePayload = "STE 6 38 255 250 255 255 255 248 255 40\r\n".getBytes();

    byte [] onTripleTap = "STE 5 22 0 255 118 0 32 253 1 250\r\n".getBytes();
    byte [] onDoubleTap = "STE 5 22 0 238 255 255 25 253 1 250\r\n".getBytes();

    private Handler mHandler = new Handler();

    private static final String TAG = "TelephonyService";

    private Boolean mIsBound;

    private Context context;

    protected PhoneIntentReceiver phoneIntentReceiver;
    protected SMSBroadcastReceiver smsBroadcastReceiver;


    private String callerNumber;
    private static boolean callStatus = false;
    public PersonalData pd = new PersonalData();

    @Override
    public void onCreate() {
        context = this;
        doBindService();

        configureSystemReceivers();

        getLockRoot();
        startSearch();

        Log.d(TAG, "onCreate called");
        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));
    }

    public void configureSystemReceivers(){
        phoneIntentReceiver = new PhoneIntentReceiver();
        phoneIntentReceiver.setReceiver(this);
        phoneIntentReceiver.setActive();

        smsBroadcastReceiver = new SMSBroadcastReceiver();
        smsBroadcastReceiver.setActivity(this);
        smsBroadcastReceiver.setActive();
    }

    protected void deactivateReceivers(){
        if(phoneIntentReceiver != null)
            phoneIntentReceiver.disable();

        if(smsBroadcastReceiver != null)
            smsBroadcastReceiver.disable();
    }

    protected void activeReceivers(){
        if(phoneIntentReceiver != null)
            phoneIntentReceiver.setActive();
        if(smsBroadcastReceiver != null)
            smsBroadcastReceiver.setActive();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public TelephonyService() {
    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            onResume();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            onPause();
            doUnbindService();
        }
    };

    @Override
    public SmsEventResult smsEventReceive(SmsEventWrapper sms) {

        writeToTargetCharacteristic(smsPayload);

        return null;
    }

    @Override
    public TelephonyEventResult incomingCallEventReceive(TelephonyWrapper telephone) {
        String status = telephone.getPhoneStatus();

        if("RINGING".equals(status)) {
            onCallStart(telephone);
        }

        if("IDLE".equals(status)) {
            onCallEnd(telephone);
        }

        return null;
    }

    @Override
    public Context getContext() {
        return this.getBaseContext();
    }

    private void onCallStart(TelephonyWrapper telephone){
        callStatus = true;
        callerNumber = telephone.getPhoneNumber();

        writeToTargetCharacteristic(callPayload);
    }

    private void onCallEnd(TelephonyWrapper telephone){
        callStatus = false;
        callerNumber = null;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeToTargetCharacteristic(afterCallPayload);
            }
        }, 800);
    }

    void onTapReceived() {
        if(callStatus) {
            phoneIntentReceiver.endCall();
            callStatus = false;
            if (callerNumber != null) {

                if (PersonalData.getRejectMsg() == null)
                    smsBroadcastReceiver.sendSMS(callerNumber, "I will call You later");
                else
                    smsBroadcastReceiver.sendSMS(callerNumber, PersonalData.getRejectMsg());
            }
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String action = bundle.getString(BleServiceIPC_Messages.ACTION);
                int resultCode = bundle.getInt(BleServiceIPC_Messages.RESULT);
                if(action==null) return;

                if(AppMessages.ACTION_TAP_RECEIVED.equals(action)){
                    onTapReceived();
                }
                if(AppMessages.ACTION_TRIPLE_TAP_RECEIVED.equals(action)){
                    onTripleTapReceived();
                }
                if(AppMessages.BLE_RSSI_UPDATED.equals(action)){
                    Integer rssi = (Integer)bundle.get(BleServiceIPC_Messages.OBJECT);
                    onRsiiUpdate(rssi);
                }
            }
        }
    };

    private void onTripleTapReceived() {

    }

    protected void onResume() {
        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));
    }

    protected void onPause() {
        unregisterReceiver(receiver);
    }

    Messenger mMessenger = GlobalActivity.mMessenger;

    public void writeToTargetCharacteristic(byte [] payload){

        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_WRITE_CHARACTERISTIC_TO_DEVICE, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putSerializable(BleServiceIPC_Messages.BYTE_ARRAY, payload);
        msg.setData(bundle);
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    //Good dont hate me about that I didn't have time (as always, especially in startups)
    protected Runnable mStatusChecker = null;
    public final static String RSSI_VALUE = "info.androidhive.actionbar.rssi_value";
    private final List<Integer> results = new ArrayList<Integer>();
    private static Boolean isTrackingState = true;
    public static long RefreshRateInMs = 1500l;
    public static final int RSSI_BUFFER_SIZE = 4;
    private Integer sum = 0;

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    private void startSearch(){
        recurrentScan();
    }

    private synchronized void recurrentScan(){

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                filterResult(null);
                if (isTrackingState)
                    recurrentScan();
            }
        }, RefreshRateInMs);
    }

    public synchronized void filterResult(final BleDevice device){
        requestUpdateTargetRSSI();
    }

    public void refresh(final Map<String, Object> args) {

        if(args.get(RSSI_VALUE)==null)
            return;
        Integer rssi_val = (Integer)args.get(RSSI_VALUE);

        if(rssi_val==null){ return;}

        if(results.size() > RSSI_BUFFER_SIZE){
            sum -= results.get(0);
            results.remove(0);
        }

        results.add(rssi_val);
        sum += rssi_val;

        double avg = (double)sum/results.size();
        autoLock(avg);
    }

    public void requestUpdateTargetRSSI(){
        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_UPDATE_TARGET_RSSI, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putString(BleServiceIPC_Messages.RSSI, "0");
        msg.setData(bundle);
        try {
            if(mMessenger != null)
                mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void onRsiiUpdate(Integer rssi){
        Map<String, Object> result = new HashMap<>();

        if(rssi==null){ return;}

        result.put(RSSI_VALUE, rssi);

        refresh(result);
    }

    DevicePolicyManager dpm;
    ComponentName mAdminName;
    public static volatile boolean locked;

    private void autoLock(Double value){
//        counter++;
        if(value < GlobalActivity.rssiRange ){

            Log.w("Device move away..", "Locking Now");
            locked = true;
            dpm.lockNow();
            writeToTargetCharacteristic(callPayload);
        } else {
            if(!locked)
                return;

            KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
            final KeyguardManager.KeyguardLock kl = km .newKeyguardLock("MyKeyguardLock");
            kl.disableKeyguard();

            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
            wakeLock.acquire();

            locked = false;
            writeToTargetCharacteristic(cleanPayload);
        }

    }

    protected void getLockRoot(){
        dpm = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminName = new ComponentName(this,MyAdmin.class);
        mHandler = new Handler();

        if(dpm.isAdminActive(mAdminName))
        {                }
        else
        {
            Intent intent1 =new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent1.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
            intent1.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Need new Admin");
            Log.w("no Admin", "Set admin");
//            startActivityForResult(intent1, REQUEST_CODE_ENABLE_ADMIN);
        }
    }


}
