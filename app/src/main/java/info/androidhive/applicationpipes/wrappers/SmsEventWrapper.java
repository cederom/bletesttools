package info.androidhive.applicationpipes.wrappers;

import android.telephony.SmsMessage;

import java.util.List;

/**
 * Created by hjacker on 03.02.15.
 */
public class SmsEventWrapper {

    List<SmsMessage> smses;

    public SmsEventWrapper(List<SmsMessage> sms){
        this.smses = sms;
    }

    public List<SmsMessage> getSmses(){
        return smses;
    }
}
