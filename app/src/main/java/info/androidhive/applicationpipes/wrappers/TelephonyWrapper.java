package info.androidhive.applicationpipes.wrappers;

/**
 * Created by hjacker on 03.02.15.
 */
public class TelephonyWrapper {

    private String phoneNumber;
    private String phoneStatus;

    public TelephonyWrapper(String phoneNumber, String phoneStatus){
        this.phoneNumber = phoneNumber;
        this.phoneStatus = phoneStatus;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneStatus() {
        return phoneStatus;
    }

}
