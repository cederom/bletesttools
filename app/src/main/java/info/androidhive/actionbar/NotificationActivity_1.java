package info.androidhive.actionbar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

import info.androidhive.applicationpipes.TelephonyEventsReceiver;
import info.androidhive.applicationpipes.result.SmsEventResult;
import info.androidhive.applicationpipes.result.TelephonyEventResult;
import info.androidhive.applicationpipes.wrappers.SmsEventWrapper;
import info.androidhive.applicationpipes.wrappers.TelephonyWrapper;

public class NotificationActivity_1 extends AbstractCommunicationActivity implements TelephonyEventsReceiver {

    byte [] emergencyPayload = "STE 3 143 122 254 0 0 132 126 149 138\r\n".getBytes();
    byte [] cleanPayload = "STE 0 0 0 0 0 0 0 0 0 0\r\n".getBytes();
    byte [] smsPayload = "STE 2 255 253 0 0 255 255 208 7 255\r\n".getBytes();
    byte [] callPayload =  "STE 3 0 255 0 255 0 255 247 135 0\r\n".getBytes();
//                              "STE 3 0 255 0 255 0 255 247 135 0"
    byte[] afterCallPayload = "STE 3 17 0 255 0 0 255 249 172 0\r\n".getBytes();

    byte [] pushPayload = "STE 2 32 67 0 243 0 11 45 60 69\r\n".getBytes();

    private static Context context;

    private static boolean callStatus = false;

    private Handler mHandler = new Handler();

    private Integer chosenPattern = 0;

    final CharSequence patterns[] = new CharSequence[] {"Sharkfin", "Reverse_Sharkfin", "Saw", "Comb", "Pulse", "Caleidoscope", "Custom" };

    private String callerNumber;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action1);
        context = this;

        doBindService();

        configurePushButtons();
        configureSliders();
        configureSystemReceivers();

    }

    private void configureSystemReceivers(){
//        phoneIntentReceiver = new PhoneIntentReceiver();
//        phoneIntentReceiver.setReceiver(this);
//
//        smsBroadcastReceiver = new SMSBroadcastReceiver();
//        smsBroadcastReceiver.setReceiver(this);
//        activeReceivers();
    }

    private void configureSliders() {

        configLongClickListener(R.id.textView15, R.id.seekBarRed);
        configLongClickListener(R.id.textView16, R.id.seekBarGreen);
        configLongClickListener(R.id.textView17, R.id.seekBarBlue);
        configLongClickListener(R.id.textView18, R.id.seekBarVib);
        configLongClickListener(R.id.textView22, R.id.seekBarLed);
        configLongClickListener(R.id.textView24, R.id.seekBarSpeed);
        configLongClickListener(R.id.textView25, R.id.seekBarTotalTime);
        configLongClickListener(R.id.textView26, R.id.seekBarSeparationTime);
    }

    protected void onDestroy() {
        deactivateReceivers();
        super.onDestroy();
    }

    /**
     * Simply set on long click on @textViewId listener: popup with input that set value of @seekBarId
     *
     * @param textViewId textView that will be put on listener
     * @param seekBarId seekBar to configure value
     */
    private void configLongClickListener(final int textViewId, final int seekBarId){
        TextView st = ((TextView) findViewById(textViewId) );
        final SeekBar sts = ((SeekBar) findViewById(seekBarId) );

        st.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text
                                        try {
                                            sts.setProgress(Integer.parseInt(userInput.getText().toString()));
                                        } catch (Exception ex) {
                                            sts.setProgress(0);
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
                return true;
            }
        });
    }

    private void configurePushButtons() {
        configPatternButton();
        configSendSms();
        configSendCall();
        configSendClear();
        configGenerateFrameButton();
    }

    private void configGenerateFrameButton() {
        final Button button = (Button) findViewById(R.id.generateFrame);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String framePayload = generateFrame();

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Your frame payload: \r\n " + framePayload);
                builder1.setCancelable(true);
                builder1.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder1.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    private void configPatternButton(){
        final Button action = (Button) findViewById(R.id.choosePatternButton);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick a Pattern");

        action.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                builder.setItems(patterns, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        chosenPattern = which + 1;

                        onPatternChosen(which);
                    }
                });
                builder.show();
            }
        });

    }

    private void onPatternChosen(int index){
        final Button button = (Button) findViewById(R.id.choosePatternButton);
        button.setText(patterns[index]);
    }

    private void configSendSms() {

        final Button action1 = (Button) findViewById(R.id.buttonPushNot);

        action1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                pushNotification();
            }
        });
    }

    private void configSendCall() {

        final Button action1 = (Button) findViewById(R.id.buttonSendCustom);

        action1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                customNotification();
            }
        });
    }

    private void configSendClear() {

        final Button action1 = (Button) findViewById(R.id.button_notification_clean);

        action1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                writeToTargetCharacteristic(cleanPayload);
            }
        });

    }

    @Override
    public SmsEventResult smsEventReceive(SmsEventWrapper sms) {

        List<SmsMessage> smses = sms.getSmses();

        for(SmsMessage s : smses){
            String text = s.getDisplayMessageBody();

            if("Help".equals(text)){
                emergencyNotification();
                return null;
            }
        }
        writeToTargetCharacteristic(smsPayload);

        return null;
    }

    @Override
    public TelephonyEventResult incomingCallEventReceive(TelephonyWrapper telephone) {

        String status = telephone.getPhoneStatus();

        if("RINGING".equals(status)) {
            onCallStart(telephone);
        }

        if("IDLE".equals(status)) {
            onCallEnd(telephone);
        }

        if(callStatus){
            sendCall();
        }

        return null;
    }

    private void onCallStart(TelephonyWrapper telephone){
        callStatus = true;
        callerNumber = telephone.getPhoneNumber();

        writeToTargetCharacteristic(callPayload);

    }

    private void onCallEnd(TelephonyWrapper telephone){
        callStatus = false;
        writeToTargetCharacteristic(cleanPayload);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeToTargetCharacteristic(afterCallPayload);
            }
        }, 800);

    }

    @Override
    public Context getContext(){
        return this.getBaseContext();
    }

    private void sendCall(){
        writeToTargetCharacteristic(callPayload);
    }

    public void pushNotification(){
        writeToTargetCharacteristic(pushPayload);

    }

    public String generateFrame(){
        Integer sLength = ((SeekBar) findViewById(R.id.seekBarSequenceLength) ).getProgress();

        Integer r = ((SeekBar) findViewById(R.id.seekBarRed) ).getProgress();
        Integer g = ((SeekBar) findViewById(R.id.seekBarGreen) ).getProgress();
        Integer b = ((SeekBar) findViewById(R.id.seekBarBlue) ).getProgress();
        Integer v = ((SeekBar) findViewById(R.id.seekBarVib) ).getProgress();

        Integer intensity = ((SeekBar) findViewById(R.id.seekBarLed) ).getProgress();
        Integer speed = ((SeekBar) findViewById(R.id.seekBarSpeed) ).getProgress();
        Integer totalTime = ((SeekBar) findViewById(R.id.seekBarTotalTime) ).getProgress();
        Integer separationTime = ((SeekBar) findViewById(R.id.seekBarSeparationTime) ).getProgress();

        String payload = "STE " + chosenPattern + " " + sLength + " " + v +" " + r +" "+ g +" "+ b +" " + intensity +" "+ speed +" "+ totalTime +" "+ separationTime + "\r\n";

        return payload;
    }

    public void customNotification(){
        String testMsg = generateFrame();

        writeToTargetCharacteristic(testMsg.getBytes());
    }

    public void emergencyNotification(){
        writeToTargetCharacteristic(emergencyPayload);
    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

    @Override
    void onTapReceived(byte [] f) {
        phoneIntentReceiver.endCall();
        if(callerNumber != null) {

            if(PersonalData.getRejectMsg() == null)
                smsBroadcastReceiver.sendSMS(callerNumber, "I will call You later");
            else
                smsBroadcastReceiver.sendSMS(callerNumber, PersonalData.getRejectMsg());
        }
    }

    @Override
    void onRtcDriverRead(byte [] f) {

    }

    @Override
    void onRtcDataRead(byte [] f) {

    }

    @Override
    void onAccDataRead(byte [] f) {

    }

    @Override
    void onAccDriverRead(byte [] f) {

    }

}


