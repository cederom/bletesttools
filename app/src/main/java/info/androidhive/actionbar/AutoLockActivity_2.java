package info.androidhive.actionbar;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import info.androidhive.actionbar.lock.MyAdmin;
import info.androidhive.communication.BleServiceIPC_Messages;

public class AutoLockActivity_2 extends AbstractTrackingActivity{

    byte [] emergencyPayload = "STE 4 90 178 160 150 0 180 200 50 50\r\n".getBytes();
    byte [] emergencyPayload2 = "STE 3 14 254 0 252 0 254 254 254 254\r\n".getBytes();
    byte [] cleanPayload = "STE 0 0 0 0 0 0 0 0 0 0\r\n".getBytes();

    /** Called when the activity is first created. */
    protected static final int REQUEST_CODE_ENABLE_ADMIN=1;
    DevicePolicyManager dpm;
    ComponentName mAdminName;
    Handler mHandler;
    Context that;

    volatile Integer rssiBoarder = -100;
    volatile Boolean locked = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lock);
        that = this;
        getLockRoot();
        configureUI();
    }

    protected void getLockRoot(){
        dpm = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminName = new ComponentName(this,MyAdmin.class);
        mHandler = new Handler();

        if(dpm.isAdminActive(mAdminName))
        {                }
        else
        {
            Intent intent1 =new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent1.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
            intent1.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Need new Admin");
            Log.w("no Admin","Set admin");
            startActivityForResult(intent1,REQUEST_CODE_ENABLE_ADMIN);
        }
    }

    protected void configureUI(){
        configureSignalSlider();
        configureLockButton();
    }

    protected void configureSignalSlider(){
        SeekBar slider = (SeekBar)findViewById(R.id.lockSlider);

        TextView rssi_boarder = (TextView) findViewById(R.id.logSliderValue);
        rssi_boarder.setText(rssiBoarder.toString());


        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rssiBoarder = (-50 - progress);

                TextView rssi_boarder = (TextView) findViewById(R.id.logSliderValue);
                rssi_boarder.setText(rssiBoarder.toString());

                GlobalActivity.rssiRange = rssiBoarder;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    protected void configureLockButton(){
        Button button=(Button)findViewById(R.id.admin);
        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {

                Log.w("Yes admin", "Locking Now");
                dpm.lockNow();

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.w("1s after: ","Now unlocking");

                        KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                        final KeyguardManager.KeyguardLock kl = km .newKeyguardLock("MyKeyguardLock");
                        kl.disableKeyguard();

                        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                                | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
                        wakeLock.acquire();
                    }
                } , 1000);
            }
        });
    }

    @Override
    void onRssiRefresh(Integer rssi, Double avgRssi) {
        TextView rssi_field = (TextView) findViewById(R.id.rssi_value);
        rssi_field.setText(rssi.toString());

        TextView avg_field = (TextView) findViewById(R.id.avg_field);
        avg_field.setText(avgRssi.toString());

        autoLock(avgRssi);
    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    static int counter = 0;
    private void autoLock(Double value){
        counter++;
        if(value < rssiBoarder ){

            Log.w("Device move away..", "Locking Now");
//            locked = true;
            dpm.lockNow();
        } else {

            KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
            final KeyguardManager.KeyguardLock kl = km .newKeyguardLock("MyKeyguardLock");
            kl.disableKeyguard();

            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
            wakeLock.acquire();

            writeToTargetCharacteristic(cleanPayload);
        }

    }


    protected void writeToTargetCharacteristic(byte [] payload){

        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_WRITE_CHARACTERISTIC_TO_DEVICE, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putSerializable(BleServiceIPC_Messages.BYTE_ARRAY, payload);
        msg.setData(bundle);
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
