package info.androidhive.actionbar;

import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import info.androidhive.applicationpipes.TelephonyEventsReceiver;
import info.androidhive.applicationpipes.result.SmsEventResult;
import info.androidhive.applicationpipes.result.TelephonyEventResult;
import info.androidhive.applicationpipes.wrappers.SmsEventWrapper;
import info.androidhive.applicationpipes.wrappers.TelephonyWrapper;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.systemevents.PhoneIntentReceiver;
import info.androidhive.systemevents.SMSBroadcastReceiver;
import info.androidhive.telephony.TelephonyService;

/**
 * Created by hjacker on 22.07.15.
 */
public class GlobalActivity extends AbstractUniqueActivity implements TelephonyEventsReceiver {

    final Context ctx = this;

    byte [] alarmPayload = "STE 2 13 0 251 1 0 92 211 111 31\r\n".getBytes();
    byte [] cleanPayload = "STE 0 0 0 0 0 0 0 0 0 0\r\n".getBytes();
    byte [] smsPayload = "STE 1 32 0 0 2 255 121 210 34 23\r\n".getBytes();
    byte [] callPayload =  "STE 1 17 255 255 255 255 109 209 173 33\r\n".getBytes();
    byte[] afterCallPayload = "STE 3 17 0 255 0 0 255 249 172 0\r\n".getBytes();
    byte[] kaleidoscopePayload = "STE 6 13 0 251 250 247 92 202 111 31\r\n".getBytes();

    private static boolean callStatus = false;

    private Handler mHandler = new Handler();

    private String callerNumber;

    public static volatile Integer rssiRange = -100;


    protected PhoneIntentReceiver phoneIntentReceiver;
    protected SMSBroadcastReceiver smsBroadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
        doBindService();

        configureButtons();
        configTelephonyReceivers();
    }

    private void configTelephonyReceivers(){
        Intent intent = new Intent(this, TelephonyService.class);
        bindService(intent, teleServiceConnection, Context.BIND_AUTO_CREATE);

        phoneIntentReceiver = new PhoneIntentReceiver();
        phoneIntentReceiver.setReceiver(this);

        smsBroadcastReceiver = new SMSBroadcastReceiver();
        smsBroadcastReceiver.setActivity(this);
    }

    protected void configureButtons(){
        Button iba = (Button)findViewById(R.id.imageButtonAlarm);
        Button ibg = (Button)findViewById(R.id.imageButtonGesture);
        Button ibn = (Button)findViewById(R.id.imageButtonNotification);
        Button ibNull = (Button)findViewById(R.id.imageButtonNull);

        Button ibActivity = (Button)findViewById(R.id.imageButtonActivity);
        Button ibAntilost = (Button)findViewById(R.id.imageButtonAntilost);

        iba.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        writeToTargetCharacteristic(alarmPayload);
                    }
                }, 5000);
                Toast.makeText(ctx, "Alarm set after 5s..", Toast.LENGTH_SHORT).show();
            }
        });
        iba.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent myIntent = new Intent(GlobalActivity.this, DateTimeActivity_7.class);
                GlobalActivity.this.startActivity(myIntent);
                return true;
            }
        });

        ibg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(ctx, "Gesture", Toast.LENGTH_SHORT).show();
            }
        });
        ibg.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent myIntent = new Intent(GlobalActivity.this, Action8Activity.class);
                GlobalActivity.this.startActivity(myIntent);
                return true;
            }
        });

        ibn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(ctx, "Notification", Toast.LENGTH_SHORT).show();
                showKaleidoscope();
            }
        });
        ibn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent myIntent = new Intent(GlobalActivity.this, NotificationActivity_1.class);
                GlobalActivity.this.startActivity(myIntent);

                return true;
            }
        });

        ibNull.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(ctx, "Nothing here", Toast.LENGTH_SHORT).show();
            }
        });
        ibNull.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent myIntent = new Intent(GlobalActivity.this, TargetAndGoalsActivity_5.class);
                GlobalActivity.this.startActivity(myIntent);

                return true;
            }
        });

        ibActivity.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(ctx, "Activity", Toast.LENGTH_SHORT).show();
            }
        });
        ibActivity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent myIntent = new Intent(GlobalActivity.this, TargetAndGoalsActivity_5.class);
                GlobalActivity.this.startActivity(myIntent);

                return true;
            }
        });

        ibAntilost.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(ctx, "Antilost", Toast.LENGTH_SHORT).show();
            }
        });
        ibAntilost.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent myIntent = new Intent(GlobalActivity.this, AutoLockActivity_2.class);
                GlobalActivity.this.startActivity(myIntent);

                return true;
            }
        });
    }

    protected void showKaleidoscope(){
        writeToTargetCharacteristic(kaleidoscopePayload);
    }

    public void emergencyNotification(){
        writeToTargetCharacteristic(alarmPayload);
    }

    @Override
    void onRssiRefresh(Integer rssi, Double avgRssi) {

    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

    @Override
    void onTapReceived() {
    }

    @Override
    void onTripleTapReceived(byte [] f) {

        String number = PersonalData.getEmergencyNumber();
        String msg = PersonalData.getEmergencyMsg();

        if(number == null || msg == null) {
            smsBroadcastReceiver.sendSMS("501551006", "Dear I need a help!");
        } else {
            smsBroadcastReceiver.sendSMS(number, msg);
        }
    }

    @Override
    void onRtcDriverRead(byte [] f) {

    }

    @Override
    void onRtcDataRead(byte [] f) {

    }

    @Override
    void onAccDataRead(byte [] f) {

    }

    @Override
    void onAccDriverRead(byte [] f) {

    }

    @Override
    public SmsEventResult smsEventReceive(SmsEventWrapper sms) {

        writeToTargetCharacteristic(smsPayload);

        return null;
    }

    @Override
    public TelephonyEventResult incomingCallEventReceive(TelephonyWrapper telephone) {
        String status = telephone.getPhoneStatus();

        if("RINGING".equals(status)) {
            onCallStart(telephone);
        }

        if("IDLE".equals(status)) {
            onCallEnd(telephone);
        }

        return null;
    }

    private void onCallStart(TelephonyWrapper telephone){
        callStatus = true;
        callerNumber = telephone.getPhoneNumber();

        writeToTargetCharacteristic(callPayload);
    }

    private void onCallEnd(TelephonyWrapper telephone){
        callStatus = false;
        callerNumber = null;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeToTargetCharacteristic(afterCallPayload);
            }
        }, 800);
    }

    @Override
    public Context getContext() {
        return null;
    }

    Messenger teleMessenger;
    Boolean isBound;
    private ServiceConnection teleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            isBound = true;

            // Create the Messenger object
            teleMessenger = new Messenger(service);

            // Create a Message
            // Note the usage of MSG_SAY_HELLO as the what value
            Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_BIND_SERVICE, 0, 0);

            // Create a bundle with the data
            Bundle bundle = new Bundle();
            bundle.putString("hello", "BleService bound");

            // Set the bundle data to the Message
            msg.setData(bundle);

            // Send the Message to the Service (in another process)
            try {
                teleMessenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // unbind or process might have crashes
            teleMessenger = null;
            isBound = false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));

        configTelephonyReceivers();

    }

    @Override
    protected void onPause() {
        super.onPause();
//        stopService(new Intent(this, TelephonyService.class));

//        unbindService(teleServiceConnection);
    }

}
