package info.androidhive.actionbar;

import java.util.Map;

/**
 * Simple interface for activity that need to refresh something
 * that is calculated (or downloaded) in background, and have to be
 * show in UI Thread
 */
public interface Refreshable {

    public void refresh(Map<String, Object> args);

}
