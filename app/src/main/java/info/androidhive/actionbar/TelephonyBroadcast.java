package info.androidhive.actionbar;

/**
 * Created by hjacker on 20.06.15.
 */
public interface TelephonyBroadcast {

    void doBroadcastCallIncomming();
}
