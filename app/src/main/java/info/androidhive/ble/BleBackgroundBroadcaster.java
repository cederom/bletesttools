package info.androidhive.ble;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.io.Serializable;
import java.util.List;

import info.androidhive.actionbar.BleBroadcast;
import info.androidhive.communication.AppMessages;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.functional.BleDevice;

/**
 * Created by hjacker on 30.05.15.
 */
public class BleBackgroundBroadcaster extends Service implements BleBroadcast {

    @Override
    public void doBroadcastScanInProgress(){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.SCAN_START);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastScanDone(List<BleDevice> discoveredDevices){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.SCAN_END);
        outputIntent.putExtra(BleServiceIPC_Messages.OBJECT, (Serializable)discoveredDevices);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastBleEnable(){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastBleNotEnable(){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.BLUETOOTH_NOT_ENABLED);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastConnectionToNonValidDevice(){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.CONNECTION_TO_NOT_VALID_DEVICE);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastSuccessfulConnectionToDevice(String deviceName){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.SUCCESSFULLY_CONNECTED_TO_DEVICE);
        outputIntent.putExtra(AppMessages.CONNECTED_TO, deviceName);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastDeviceDisconnected(String deviceName){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.BLE_DEVICE_DISCONNECTED);
        sendBroadcast(outputIntent);
    }

    @Override
    public void odBroadcastOnRssiUpdate(Integer rssi){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.BLE_RSSI_UPDATED);
        outputIntent.putExtra(BleServiceIPC_Messages.OBJECT, (Serializable)rssi);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastOnTapDetected(byte [] f){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACTION_TAP_RECEIVED);
        outputIntent.putExtra(BleServiceIPC_Messages.BYTE_ARRAY, (Serializable)f);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastOnTripleTapDetected(byte [] f) {
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACTION_TRIPLE_TAP_RECEIVED);
        outputIntent.putExtra(BleServiceIPC_Messages.BYTE_ARRAY, (Serializable)f);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastOnReadPedometerData(byte [] bytes){
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACC_PEDOMETER_TARGET_UPDATED);
        outputIntent.putExtra(BleServiceIPC_Messages.OBJECT, (Serializable)bytes);
        sendBroadcast(outputIntent);
    }

    @Override
    public void doBroadcastOnReadPedometerDriver(byte[] bytes) {
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACTION_ACC_DRIVER_READ);
        outputIntent.putExtra(BleServiceIPC_Messages.OBJECT, (Serializable)bytes);
        sendBroadcast(outputIntent);

    }

    @Override
    public void doBroadcastOnReadBleDriver(byte[] bytes) {
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACTION_BLE_DRIVER_READ);
        outputIntent.putExtra(BleServiceIPC_Messages.OBJECT, (Serializable)bytes);
        sendBroadcast(outputIntent);

    }

    @Override
    public void doBroadcastOnReadRtcDriver(byte[] bytes) {
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACTION_RTC_DRIVER_READ);
        outputIntent.putExtra(BleServiceIPC_Messages.OBJECT, (Serializable)bytes);
        sendBroadcast(outputIntent);

    }

    @Override
    public void doBroadcastOnReadRtcData(byte[] bytes) {
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACTION_RTC_DATA_READ);
        outputIntent.putExtra(BleServiceIPC_Messages.BYTE_ARRAY, (Serializable)bytes);
        sendBroadcast(outputIntent);

    }

    @Override
    public void doBroadcastOnFlashMemoryData(byte[] bytes) {
        Intent outputIntent = new Intent(BleServiceIPC_Messages.NOTIFICATION_CODE);
        outputIntent.putExtra(BleServiceIPC_Messages.RESULT, AppMessages.BLUETOOTH_ENABLED);
        outputIntent.putExtra(BleServiceIPC_Messages.ACTION, AppMessages.ACTION_FLASH_DATA_READ);
        outputIntent.putExtra(BleServiceIPC_Messages.BYTE_ARRAY, (Serializable)bytes);
        sendBroadcast(outputIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new BleBackgroundBinder();

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class BleBackgroundBinder extends Binder {
        BleBackgroundBroadcaster getService() {
            return BleBackgroundBroadcaster.this;
        }
    }

}
