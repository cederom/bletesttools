package info.androidhive.ble;

/**
 * Created by hjacker on 17.05.15.
 */
public interface BytesBufferRing {

    void processInput(final byte [] buffer, final int size);

    int length();

}
