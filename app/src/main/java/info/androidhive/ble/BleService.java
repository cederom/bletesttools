package info.androidhive.ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import info.androidhive.actionbar.R;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.functional.BleDevice;
import info.androidhive.functional.CyclicBufferProcessor;

public class BleService extends Service {

    private static final String TAG = "BleService";

    // Messenger object used by clients to send messages to IncomingHandler
    Messenger mMessenger = new Messenger(new IncomingHandler());

    private Context context;

    //Other application specific dependencies
    private BytesBufferRing processBuffer ;

    private BleBackgroundBroadcaster mBoundBroadcaster;
    private Boolean mIsBound;
    /**
     * BLE fields
     */
    private List<BleDevice> discoveredDevices = new ArrayList<>();

    //Target service that we trying to connect
    public static final String TARGET_SERVICE_UUID        = "1d5688de-866d-3aa4-ec46-a1bddb37ecf6";
    public static final String TARGET_CHARACTERISTIC_UUID = "af20fbac-2518-4998-9af7-af42540731b3";

    // Stops scanning after SCAN_PERIOD seconds.
    public static final long SCAN_PERIOD = 3000;

    private BleDevice myDevice;

    private BluetoothAdapter mBluetoothAdapter;

    private BluetoothGatt mConnectedGatt;

    private Handler mHandler = new Handler();

    /**
     * Android Services stuff
     */
    public BleService() {};

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
        bindService(new Intent(context, BleBackgroundBroadcaster.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            mBoundBroadcaster = ((BleBackgroundBroadcaster.BleBackgroundBinder)service).getService();
            processBuffer = new CyclicBufferProcessor(mBoundBroadcaster);
            // Tell the user about this for our demo.
            Toast.makeText(context, R.string.broadcasts_connected, Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            mBoundBroadcaster = null;
            Toast.makeText(context, R.string.broadcasts_disconnected, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onCreate() {
        context = this;
        startService(new Intent(this, BleBackgroundBroadcaster.class));
        doBindService();
        Log.d(TAG, "onCreate called");
    }

    /*
    Return our Messenger interface for sending messages to
    the service by the clients.
    */
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind done");
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // Tell the user we unbind.
        Toast.makeText(this, R.string.ble_service_unbind, Toast.LENGTH_SHORT).show();

        return false;
    }

    @Override
    public void onDestroy() {

        // Tell the user we stopped.
        Toast.makeText(this, R.string.ble_service_stopped, Toast.LENGTH_SHORT).show();
    }

    /**
     * BLE Functionalities
     */
    public BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            Log.i("BLE service", "onLeScan - device: " + device.getAddress() + " - rssi: " + rssi);

            boolean toAdd = true;

            for(BleDevice d : discoveredDevices){
                if(d.getMacAddress().equals(device.getAddress()) && d.getDeviceName().equals(device.getName())){
                    toAdd = false;
                    break;
                }
            }
            if(toAdd){
                discoveredDevices.add(new BleDevice(device, rssi, scanRecord));
            }

        }
    };

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.i(TAG, " onDescriptorWrite - status: " + status + "  - UUID: " + descriptor.getUuid());
        }

        @Override
        public void onReadRemoteRssi (BluetoothGatt gatt, int rssi, int status){
            Log.d(TAG, "On Read Remote Rssi: "+status+" rssi -> " + rssi );

            myDevice.updateRSSIvalue(rssi);

            odBroadcastOnRssiUpdate();
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "Connection State Change: " + status + " -> " + connectionState(newState));
            mConnectedGatt = gatt;
            if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED) {
                /*
                 * Once successfully connected, we must next discover all the services on the
                 * device before we can read and write their characteristics.
                 */
                gatt.discoverServices();
            } else if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_DISCONNECTED) {
                /*
                 * If at any point we dropCurrentConnection
                 */
                gatt.disconnect();
                myDevice = null;
            } else if (status != BluetoothGatt.GATT_SUCCESS) {
                /*
                 * If there is a failure at any stage, simply dropCurrentConnection
                 */
                gatt.disconnect();
                gatt.close();
                cleanUpAfterDisconnect();
            }
        }

        private void cleanUpAfterDisconnect(){
            String name = myDevice.getDeviceName();
            myDevice = null;
            doBroadcastDeviceDisconnected(name);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.d(TAG, "This is on  onCharacteristicChanged");

            processBuffer.processInput(characteristic.getValue(), characteristic.getValue().length);
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(TAG, "onServicesDiscovered Services Discovered: " + status);
            //Lets validate device that we trying to connect it must have our specific service and characteristic
            Boolean validDevice = false;

            //Now we can start reading/writing characteristics
            List<BluetoothGattService> services = gatt.getServices();
            for (BluetoothGattService service : services) {
                //Connect to particular service
                if (service.getUuid().equals(UUID.fromString(TARGET_SERVICE_UUID))) {

                    BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(TARGET_CHARACTERISTIC_UUID));
                    if(characteristic != null)
                        validDevice=true;

                    if(myDevice!= null && !myDevice.isInDataMode() ){
                        myDevice.setTrackingCharacteristic(characteristic);
                        turnInDataMode();
                    }
                }
            }

            if(validDevice && !myDevice.isInDataMode()) {
                myDevice.setBleServices(services);
            } else if( !validDevice) {
                doBroadcastConnectionToNonValidDevice();
            }

        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i(TAG, "onCharacteristicWrite - status: " + status + "  - UUID: " + characteristic.getUuid());
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i(TAG, "onCharacteristicRead - status: " + status + "  - UUID: " + characteristic.getUuid());
        }

        private String connectionState(int status) {
            switch (status) {
                case BluetoothProfile.STATE_CONNECTED:
                    return "Connected";
                case BluetoothProfile.STATE_DISCONNECTED:
                    return "Disconnected";
                case BluetoothProfile.STATE_CONNECTING:
                    return "Connecting";
                case BluetoothProfile.STATE_DISCONNECTING:
                    return "Disconnecting";
                default:
                    return String.valueOf(status);
            }
        }
    };

    public synchronized void connectWithDevice(final BleDevice device) {
        myDevice = device;

        if(mConnectedGatt != null){
            mConnectedGatt = device.getDevice().connectGatt(context, false, mGattCallback);
        }else {
            device.getDevice().connectGatt(context, false, mGattCallback).connect();
        }

    }

    public boolean writeToTargetCharacteristic(final byte [] newValue){
        if(myDevice!=null) {

            performOnBackgroundThread(new Runnable() {
                @Override
                public void run() {
                    BluetoothGattCharacteristic myCharacteristic = myDevice.getTrackingCharacteristic();
                    myCharacteristic.setValue(newValue);
                    mConnectedGatt.writeCharacteristic(myCharacteristic);
                }
            });
        }

        return true;
    }

    public void turnInDataMode(){
        Log.i(TAG, "turnInDataMode() - myDevice.isInDataMode(): " + myDevice.isInDataMode());
        if(!myDevice.isInDataMode()) {
            myDevice.setDataMode(true);

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    BluetoothGattCharacteristic trackingCharacteristic = myDevice.getTrackingCharacteristic();

                    mConnectedGatt = myDevice.getDevice().connectGatt(context, false, mGattCallback);
                    //Without this delay application behaviour can be undefined
                    // - it can simply write or not CharacteristicNotification no rule
                    try { Thread.sleep(200);} catch (InterruptedException e) { e.printStackTrace(); }

                    mConnectedGatt.setCharacteristicNotification(trackingCharacteristic, true);

                    List<BluetoothGattDescriptor> descArray = trackingCharacteristic.getDescriptors();

                    for (BluetoothGattDescriptor lastDescriptor : descArray)
                        if (lastDescriptor.getCharacteristic() == trackingCharacteristic) {
                            //BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.ENABLE_INDICATION_VALUE;
                            lastDescriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                            mConnectedGatt.writeDescriptor(lastDescriptor);
                        }
                }
            }, 1000);


            doBroadcastSuccessfulConnectionToDevice(myDevice.getDeviceName() + " " + myDevice.getMacAddress());

        }

    }

    private void scanLeDevice(final boolean enable) {
        scanLeDevice(enable, SCAN_PERIOD);
    }

    private void scanLeDevice(final boolean enable, final long ms) {
        if (enable) {
            discoveredDevices = new ArrayList<BleDevice>();
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBluetoothAdapter.stopLeScan(leScanCallback);
                    doBroadcastScanDone();
                }
            }, ms);

            mBluetoothAdapter.startLeScan(leScanCallback);
        } else {
            mBluetoothAdapter.stopLeScan(leScanCallback);
        }
    }

    private void doScan(){
        discoveredDevices.clear();
        BluetoothManager manager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            doBroadcastBleNotEnable();
            return;
        }

        scanLeDevice(true);
    }

    public void updateTargetRSSI(){
        mConnectedGatt.readRemoteRssi();
    }

    /**
     * Messenger and IPC functionalities
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BleServiceIPC_Messages.MSG_BIND_SERVICE:
                    onServiceBind(msg);
                    break;
                case BleServiceIPC_Messages.MSG_SCAN_BLE_DEVICES:
                    onBleScanMsg(msg);
                    break;
                case BleServiceIPC_Messages.MSG_CONNECT_TO_DEVICE:
                    onConnectionRequest(msg);
                    break;
                case BleServiceIPC_Messages.MSG_WRITE_CHARACTERISTIC_TO_DEVICE:
                    onWriteCharacteristicToDevice(msg);
                    break;
                case BleServiceIPC_Messages.MSG_READ_CHARACTERISTIC_FROM_DEVICE:
                    onReadCharacteristicToDevice(msg);
                    break;
                case BleServiceIPC_Messages.MSG_DISCONNECT_FROM_DEVICE:
                    onDisconnectFromDevice(msg);
                    break;
                case BleServiceIPC_Messages.MSG_UPDATE_TARGET_RSSI:
                    onRsiiUpdate(msg);
                    break;

                default:
                    super.handleMessage(msg);
            }

        }

        public void onServiceBind(Message msg){
            Bundle bundle = msg.getData();
            String hello = (String) bundle.get("hello");

            Toast.makeText(getApplicationContext(), hello, Toast.LENGTH_SHORT).show();
        }

        public void onBleScanMsg(Message msg){
            doScan();
        }

        public void onConnectionRequest(Message msg){
            Bundle bundle = msg.getData();
            String mac = (String) bundle.get(BleServiceIPC_Messages.MAC_ADDRESS);
            if(mac == null) return;
            for(BleDevice d : discoveredDevices){
                if(d.getMacAddress().equals(mac))
                    connectWithDevice(d);
            }
        }

        public void onWriteCharacteristicToDevice(Message msg){
            Bundle bundle = msg.getData();
            byte [] value = (byte []) bundle.get(BleServiceIPC_Messages.BYTE_ARRAY);
            if(value == null) return;

            writeToTargetCharacteristic(value);
        }

        public void onReadCharacteristicToDevice(Message msg){

        }

        public void onDisconnectFromDevice(Message msg){

        }

        public void onRsiiUpdate(Message msg){
            updateTargetRSSI();
        }
    }

    /*
    * Broadcasts -> Result(Feedback) of action from activity
    *              ->They all have to be handled in MainActivity
    */
    public void doBroadcastScanInProgress(){
        mBoundBroadcaster.doBroadcastScanInProgress();
    }

    public void doBroadcastScanDone(){
        mBoundBroadcaster.doBroadcastScanDone(discoveredDevices);
    }

    public void doBroadcastBleEnable(){
        mBoundBroadcaster.doBroadcastBleEnable();
    }

    public void doBroadcastBleNotEnable(){
        mBoundBroadcaster.doBroadcastBleNotEnable();
    }

    public void doBroadcastConnectionToNonValidDevice(){
        mBoundBroadcaster.doBroadcastConnectionToNonValidDevice();
    }

    public void doBroadcastSuccessfulConnectionToDevice(String deviceName){
        mBoundBroadcaster.doBroadcastSuccessfulConnectionToDevice(deviceName);
    }

    public void doBroadcastDeviceDisconnected(String deviceName){
        mBoundBroadcaster.doBroadcastDeviceDisconnected(deviceName);
    }

    public void odBroadcastOnRssiUpdate(){
        mBoundBroadcaster.odBroadcastOnRssiUpdate(myDevice.getLatestRssi());
    }

    /*
     * BLE Asynchronous jobs
     */
    public static Thread performOnBackgroundThread(final Runnable runnable) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {
                    Log.d(TAG, "End");
                }
            }
        };
        t.start();
        return t;
    }
}